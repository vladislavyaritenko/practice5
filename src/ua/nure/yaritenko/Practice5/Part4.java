package ua.nure.yaritenko.Practice5;


import java.util.ArrayList;
import java.util.Random;

class MyThread4 implements Runnable {
    private int[] str;
    static final ArrayList<Integer> al = new ArrayList<>();

    MyThread4(int[] c) {
        str = c;
    }

    public void run() {
        int max = 0;
        for (int i : str) {
            if (max < str[i]) {
                max = str[i];
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (al) {
            al.add(max);
        }

    }
}

public class Part4 {
    private static int[][] setMtrix(final int M, final int N) {
        Random random = new Random();
        int[][] matrix = new int[M][N];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                matrix[i][j] = random.nextInt(100);
            }
        }
        return matrix;
    }

    public static void main(String[] args) {
        final int M = 4;
        final int N = 100;
        int maxValue = 0;
        long start;
        long end;
        int[][] matrix = Part4.setMtrix(M, N);
        ArrayList<Thread> threads = new ArrayList<>();
        start = System.currentTimeMillis();
        for (int i = 0; i < M; i++) {
            threads.add(new Thread(new MyThread4(matrix[i])));
        }
        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (Integer i : MyThread4.al) {

            if (maxValue < i) {
                maxValue = i;
            }
        }
        end = System.currentTimeMillis();
        System.out.println("Максимальное значение при параллельном вычисление: " + maxValue);
        System.out.println("Время вычисления: " + (end - start) + " мс.");
        long start2;
        long end2;
        int maxValue2 = 0;
        start2 = System.currentTimeMillis();
        for (int[] i : matrix) {
            for (int j : i) {
                if (maxValue2 < j) {
                    maxValue2 = j;
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
        end2 = System.currentTimeMillis();
        System.out.println("Максимальное значение без параллельного вычисление: " + maxValue2);
        System.out.println("Время вычисления: " + (end2 - start2) + " мс.");
    }
}
