package ua.nure.yaritenko.Practice5;

class MyThread3 implements Runnable {
    Counter c;

    MyThread3(Counter c) {
        this.c = c;
    }

    private void compare() {
        System.out.println(c.count1 == c.count2);
        c.count1++;
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        c.count2++;

    }

    public void run() {
        int value = 0;
        while (value < 20) {
            synchronized (c) {
                compare();
                value++;
            }
        }
    }
}

class Counter {
    int count1, count2;

    Counter() {
        this.count1 = 0;
        this.count2 = 0;
    }
}

public class Part3 {
    public static void main(String[] args) {
        Counter c = new Counter();
        for (int i = 0; i < 5; i++) {
            new Thread(new MyThread3(c)).start();
        }

    }
}
