package ua.nure.yaritenko.Practice5;

import java.io.*;
import java.util.ArrayList;

class MyThread5 implements Runnable {
    private char ch;
    private final String nameFile;
    private RandomAccessFile raf;
    private long cur;

    MyThread5(char ch, String nameFile, RandomAccessFile raf, long cur) {
        this.ch = ch;
        this.nameFile = nameFile;
        this.raf = raf;
        this.cur = cur;
        try {
            raf.seek(this.cur + 20);
            raf.write(System.lineSeparator().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        for (int i = 0; i < 20; i++) {
            synchronized (nameFile) {
                try {
                    raf.seek(cur);
                    raf.write(ch);
                    cur = raf.getFilePointer();
                    if (i == 19) {
                        cur += 2;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

public class Part5 {
    public static void main(String[] args) throws IOException {
        String nameFile = "part5.txt";
        File file = new File("part5.txt");
        ArrayList<Thread> threads = new ArrayList<>();
        if (file.exists()) {
            file.delete();
            System.out.println("Файл " + file + " удален!");
        }
        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        System.out.println("Файл " + file + " создан!");
        long[] position = {0L, 22L, 44L, 66L, 88L, 110L, 132L, 154L, 176L, 198L};
        for (int i = 0; i < 10; i++) {
            char ch = (char) ('0' + i);
            threads.add(new Thread(new MyThread5(ch, nameFile, raf, position[i])));
        }
        for (Thread t : threads) {
            t.start();
        }
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        BufferedReader fin = new BufferedReader(new FileReader(file));
        String line;
        while ((line = fin.readLine()) != null) {
            System.out.println(line);
        }
    }
}
