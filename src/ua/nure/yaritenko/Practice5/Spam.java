package ua.nure.yaritenko.Practice5;

import java.util.ArrayList;
import java.util.Scanner;


class MyThread2 implements Runnable {
    int time;
    String massage;

    MyThread2(int time, String massage) {
        this.time = time;
        this.massage = massage;
    }

    public void run() {
        Thread t = Thread.currentThread();
        while (!t.isInterrupted()) {
            System.out.println(massage);
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}

public class Spam {
    int[] time;
    String[] massage;
    ArrayList<Thread> threads;

    Spam(int[] time, String[] massage) {
        this.time = time;
        this.massage = massage;
        threads = new ArrayList<>();
    }

    void stop() {
        for (Thread t : threads) {
            t.interrupt();
        }
    }

    void start() {
        for (int i = 0; i < time.length; i++) {
            threads.add(new Thread(new MyThread2(time[i], massage[i])));
        }
        for (Thread t : threads) {
            t.start();
        }
    }

    public static void main(String[] args) {
        int[] time = {333, 555};
        String[] massage = {"AAAAAA", "bbb"};
        Spam spam = new Spam(time, massage);
        spam.start();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if ("".equals(line)) {
                spam.stop();
                break;
            }
        }
    }
}

