package ua.nure.yaritenko.Practice5;

import java.io.IOException;
import java.io.InputStream;

public class Part2 {
    public static void main(String[] args) {
        final byte[] b = System.lineSeparator().getBytes();
        InputStream is = new InputStream() {
            int cur;
            public int read() throws IOException {
                if(cur == 0){
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return cur < b.length ? b[cur++] : -1;
            }
        };
        System.setIn(is);
        Spam.main(args);



    }
}
