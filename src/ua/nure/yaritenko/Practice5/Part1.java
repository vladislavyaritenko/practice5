package ua.nure.yaritenko.Practice5;

class MyThread1 implements Runnable {
    public void run() {
        int count = 0;
        while (count < 6) {
            System.out.println(Thread.currentThread().getName());

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
        }

    }
}

class MyThread extends Thread {
    public void run() {
        int count = 0;
        while (count < 6) {
            System.out.println(Thread.currentThread().getName());
            try {
                Thread.sleep(500);
                count++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Part1 {
    public static void main(String[] args) {
        new Thread(new MyThread1()).start();
        new MyThread().start();
    }
}

